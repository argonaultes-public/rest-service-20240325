package fr.argonaultes.restservice;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class RestServiceEmptyTest {

    @Autowired
    private GreetingController controller;

    @Test
    public void testEmpty() {
        assertNotNull(controller.empty());
    }

}
