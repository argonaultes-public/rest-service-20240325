package fr.argonaultes.restservice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class RestIT {

    @Test
    void testIfDbExistsFailed() {
        assertNotNull(System.getenv("DATABASE_URL_TEST"), "Missing env variable DATABASE_URL_TEST");
    }

    @Test
    void testIfDbExistsError() {
        if (System.getenv("DATABASE_URL_TEST").isBlank()) {
            fail("Missing env variable DATABASE_URL_TEST");
        }
    }

}
